(require 'seq)

;;; Code:

(defun is_prime (num)
  (cl-labels
      ((wrap (cur num limit)
	     (cond
	      ((> cur limit) t)
	      ((<= cur limit)
	       (if (eq (% num cur) 0)
		   nil
		 (wrap (+ cur 1) num limit)
		 )
	       )
	      )
	     )
       )

    (wrap 2 num (sqrt num))
    )
  )

(defun is_prime_2 (num)
  (cl-labels
      ((wrap (cur num limit)
	     (cond
	      ((> cur limit)
	       t
	       )
	      ((<= cur limit)
	       (if (eq (% num cur) 0)
		   nil
		 (wrap (+ cur 2) num limit)
		 )
	       )
	      )
	     ))
    (if (eq num 2)
	t
      (wrap 3 num (sqrt num))
      )
    )
  )

(defun next_prime (num)
  "Return next prime after num"
  (if (eq num 2)
      3
    (if (is_prime_2 (+ num 2))
	(+ num 2)
      (next_prime (+ num 2))
      )
    )
  )

(defun prime_factors (num)
  (cl-labels
      ((wrap (factor current working)
	     (cond
	      ((eq factor working)
	       (cons factor current))
	      ((eq (% working factor) 0)
	       (wrap factor (cons factor current) (/ working factor)))
	      (t
	       (wrap (next_prime factor) current working))
	      )
	     ))
    (wrap 2 '() num)
    )
  )

(seq-reduce 'max (prime_factors 600851475143) 0)

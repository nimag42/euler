(require 'seq)

;;; Code:

(defun sum_list (list)
  "Sum a LIST of integers."
  (if (null list)
      0
    (+
     (car list)
     (sum_list (cdr list))
     )
    )
  )

(defun fibo (list max)
  " Return fibo with last value <= max"
  (if (> (car list) max)
      (cdr list)
    (fibo
     (cons
      (+ (car list) (car (cdr list)))
      list
      )
     max
     )
    )
  )

(defun is_even (num)
  (eq (% num 2) 0)
  )

(insert (number-to-string
	 (sum_list
	  (seq-filter
	   'is_even
	   (fibo '(1 1) 4000000)
	   )
	  )
	 )
	)

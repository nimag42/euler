
(defun is_prime_2 (num)
  (cl-labels
      ((wrap (cur num limit)
	     (cond
	      ((> cur limit)
	       t
	       )
	      ((<= cur limit)
	       (if (eq (% num cur) 0)
		   nil
		 (wrap (+ cur 2) num limit)
		 )
	       )
	      )
	     ))
    (if (eq num 2)
	t
      (wrap 3 num (sqrt num))
      )
    )
  )

(defun next_prime (num)
  "Return next prime after num"
  (if (eq num 2)
      3
    (if (is_prime_2 (+ num 2))
	(+ num 2)
      (next_prime (+ num 2))
      )
    )
  )

(defun prime_factors (num)
  (cl-labels
      ((wrap (factor current working)
	     (cond
	      ((eq factor working)
	       (setf (alist-get factor current) (1+ (or (alist-get factor current) 0)))
	       current)
	      ((eq (% working factor) 0)
	       (setf (alist-get factor current) (1+ (or (alist-get factor current) 0)))
	       (wrap factor current (/ working factor)))
	      (t
	       (wrap (next_prime factor) current working))
	      )
	     ))
    (wrap 2 '() num)
    )
  )

(setq multiples_until_20
      (let ((primes '()))
	(dotimes (i 19 primes)
	      (dolist (el (prime_factors (+ i 2)) primes)
		(setf
		 (alist-get (car el) primes)
		 (max (cdr el) (or (alist-get (car el) primes) 0)))))))

(defun mul_factors (factors)
  (seq-reduce '(lambda (x y)
		 (*
		  x
		  (expt (car y) (cdr y))))
	      factors
	      1
	      ))

(insert (number-to-string (mul_factors multiples_until_20)))


(setq max-specpdl-size 3000000000)
(setq max-lisp-eval-depth 300000000)

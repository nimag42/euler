(require 'seq)

;;; Code:

(defun invert_num (num)
  (cl-labels
      ((wrap (rest build)
	     (if (< rest 10)
		 (+ rest build)
	       (wrap (/ rest 10) (* (+ (% rest 10) build) 10))
	       )
	     ))
    (wrap num 0)
    )
  )

(defun is_palindrom (num)
  (eq (invert_num num) num)
  )

(defun check_num (num1)
  (let ((count 999)
	(palin nil))
    (while (>= count 100)
      (if (is_palindrom (* num1 count))
	  (push (* num1 count) palin)
	)
      (setq count (1- count))
      )
    palin
    )
  )

(check_num 995)

(let ((num1 999)
      (palin '(1 2)))
  (while (>= num1 100)
    (let ((ret (check_num num1)))
      (if ret
	  (setq palin (append ret palin))
	)
      (setq num1 (1- num1))
      )
    )
  (insert (number-to-string (seq-reduce 'max palin 0)))
  )

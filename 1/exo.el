;;; Code:

(defun sum_list (list)
  "Sum a LIST of integers."
  (if (null list)
      0
    (+
     (car list)
     (sum_list (cdr list))
     )
    )
  )

(defun construct_multiple (multiple number)
  "Construct a list of multiple of MULTIPLE until NUMBER."
  (cond
   ((<= number 0) '())
   ((> number 0)
    (if (eq (% number multiple) 0)
	(cons number (construct_multiple multiple (- number 1)))
      (construct_multiple multiple (- number 1))
      )
    )
   )
  )

(defun exo1 (limit)
  "Solution to exo 1"
  (insert
   (number-to-string
    (sum_list
     (delete-dups
      (append
       (construct_multiple 5 (- limit 1))
       (construct_multiple 3 (- limit 1))
       )
      )
     )
    )
   )
  )

(exo1 1000)
(construct_multiple 5 1000)

(setq max-specpdl-size 300000)
(setq max-lisp-eval-depth 200000)
(setq debug-on-error t)
